package pers.hqg.mqproduct.sendAndReceive;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;


@Configuration
public class MQConfig {

      @Bean
      public Queue fanoutQueue(){
          Map<String,Object> arguments =  new HashMap<>();
          arguments.put(QueueConstants.DEAD_LETTER_EXCHANGE,QueueConstants.DEAD_EXCHANGE);
          arguments.put(QueueConstants.DEAD_LETTER_ROUTING_KEY,QueueConstants.DEAD_ROUTING_KEY);
          return  new Queue(QueueConstants.FANOUT_QUEUE,true,false,false,arguments);
      }

      @Bean
      public FanoutExchange fanoutExchange(){
            return new FanoutExchange(QueueConstants.FANOUT_EXCHANGE_NAME);
      }

      @Bean
      public Binding bindingExchange(){
           return BindingBuilder.bind(fanoutQueue()).to(fanoutExchange());
      }




      //死信队列
      @Bean
      public  Queue deadQueue(){
         return  new Queue(QueueConstants.DEAD_QUEUE);
      }

      @Bean
      public DirectExchange deadExchange(){
          return new DirectExchange(QueueConstants.DEAD_EXCHANGE);
      }

      @Bean
      public Binding  bindingDeadExchange(){
          return  BindingBuilder.bind(deadQueue()).to(deadExchange()).with(QueueConstants.DEAD_ROUTING_KEY);
      }


      //延时插件使用
      @Bean
      public Queue delayQueue(){
           return  new Queue("d_queue",true);
      }

      @Bean
      public CustomExchange delayExchange(){
          Map<String,Object> args = new HashMap<>();
          args.put("x-delayed-type","direct");
          return new CustomExchange("d_exchange","x-delayed-message",true,false,args);
      }

      @Bean
      public Binding delayBinding(){
          return BindingBuilder.bind(delayQueue()).to(delayExchange()).with("delay_key").noargs();
      }
}
