package pers.hqg.mqproduct.sendAndReceive;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class MQSender {

      private static  final Logger logger = LoggerFactory.getLogger(MQSender.class);


      @Autowired
      public RabbitTemplate rabbitTemplate;


      public void send(String args){
          //设置消息发送到交换机 异常回调函数 ConfirmCallback
          rabbitTemplate.setConfirmCallback(((correlationData, isack, cause) -> {
              logger.info("本次消息的唯一标识是："+correlationData);
              logger.info("是否存在消息拒绝接收："+isack);
              if(isack){
                  logger.info("拒绝接收的理由是："+cause);
              }else{
                  logger.info("消息发送成功");
              }
          }));

          //设置消息成功发送到交换机后，没有成功分配到队列的 异常回调函数 ReturnCallback
          rabbitTemplate.setReturnsCallback(returnedMessage -> {
              logger.info("发送错误:{}",returnedMessage);
          });
          //延时处理-死信队列(autoACK)
//          rabbitTemplate.convertAndSend(QueueConstants.FANOUT_EXCHANGE_NAME,"",args,message -> {
////              message.getMessageProperties().setExpiration(String.valueOf(10 * 1000));
//              message.getMessageProperties().setDelay(5000);
//              return message;
//          });
          //延迟处理-延迟插件(手动ACK)
          rabbitTemplate.convertAndSend(
                  "d_exchange",
                  "delay_key",
                  args,
                  message -> {
                     message.getMessageProperties().setDelay(60 * 1000);
                     return message;
          });
          logger.info("消息已发送：{}",args+"|"+ LocalDateTime.now());

      }
}
