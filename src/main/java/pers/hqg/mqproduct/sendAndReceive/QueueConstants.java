package pers.hqg.mqproduct.sendAndReceive;

public class QueueConstants {

    public static final  String  DEAD_QUEUE = "dead_queue";
    public static final  String  DEAD_ROUTING_KEY = "dead_routing_key";
    public static final  String  DEAD_EXCHANGE = "dead_exchange";

    public static final  String  DEAD_LETTER_EXCHANGE = "x-dead-letter-exchange";
    public static final  String  DEAD_LETTER_ROUTING_KEY = "x-dead-letter-routing-key";

    public  static final String  FANOUT_QUEUE = "fanout_queue";
    public  static final String  FANOUT_EXCHANGE_NAME = "fanoutExchange";



}
