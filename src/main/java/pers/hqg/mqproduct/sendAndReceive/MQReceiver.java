package pers.hqg.mqproduct.sendAndReceive;

import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Map;

@Component
@EnableRabbit
@Configuration
public class MQReceiver {


      private static final Logger logger = LoggerFactory.getLogger(MQReceiver.class);


      @RabbitListener(queues = "d_queue")
      public void dRecevier(String args, Message message, Channel channel, @Headers Map<String,Object> headers) throws IOException {
            logger.info("接收内容:{}",args);
             //通知MQ消息已被接收,可以ACK（从队列中删除）
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
            try {
                  logger.info("result:{}",args);
                  //手动ack
                  Long deliveryTag  = (Long) headers.get(AmqpHeaders.DELIVERY_TAG);
                  //手动签收   true:表示重新放回队列  false:从队列中删除
                  //deliveryTag : 唯一标识
                  channel.basicAck(deliveryTag,false);
            }catch (Exception e){
                  logger.error(e.getMessage());
                  //消费失败，尝试消息补发再次消费
                  /**
                   * basicRecover方法是进行补发操作，
                   * 其中的参数如果为true是把消息退回到queue但是有可能被其它的consumer(集群)接收到，
                   * 设置为false是只补发给当前的consumer
                   */
                  channel.basicRecover(false);
            }


      }


      @RabbitListener(queues = QueueConstants.DEAD_QUEUE)
      public void rece(String args){
            logger.info("死信队列接收消息:{}",args);
      }
}
