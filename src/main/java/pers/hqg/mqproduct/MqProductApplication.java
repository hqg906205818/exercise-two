package pers.hqg.mqproduct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MqProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(MqProductApplication.class, args);
    }

}
