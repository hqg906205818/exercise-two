package pers.hqg.mqproduct.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.hqg.mqproduct.sendAndReceive.MQReceiver;
import pers.hqg.mqproduct.sendAndReceive.MQSender;

@RestController
@RequestMapping("/MQ")
public class MQController {
    @Autowired
    MQSender sender;
    @Autowired
    MQReceiver receiver;

    @RequestMapping("/send/{args}")
    public void send(@PathVariable String args){
           sender.send(args);
    }
}
